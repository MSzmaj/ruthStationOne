#!/usr/bin/python
from PyQt4 import QtGui, QtCore

class Keyboard(QtGui.QMainWindow):
    def __init__(self, app, textBox=None, isPassword=False, password=None, defaultValues=None):
        super(Keyboard, self).__init__()
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.isPassword = isPassword
        if password is not None:
            self.passwordObject = password
            self.passwordText = password.password
        self.originalText = textBox.text()
        self.mainText = self.originalText
        self.mainTextBox = textBox
        self.app = app
        self.shiftCheck = False
        self.currentCase = 0
        self.setUpKeys()
        self.setRows()
        self.currentKey = self.TildaKey
        screen_resolution = app.desktop().screenGeometry()
        screenWidth, screenHeight = screen_resolution.width(), screen_resolution.height()
        self.setGeometry((screenWidth / 2) - (890 / 2), screenHeight - self.height() / 1.5, 890, 285)
        self.setWindowTitle("Keyboard")
        self.setStyleSheet("background-color: rgba(26,75,71,255)")
        if defaultValues is not None:
            if self.originalText in defaultValues:
                self.mainTextBox.setText("")
                self.mainText = ""
        self.show()

    def setUpKeys(self):
        self.TildaKey = Key(self, "`", "~", "`", "~", 50, True)
        self.OneKey = Key(self, "1", "!", "1", "!")
        self.TwoKey = Key(self, "2", "@", "2", "@")
        self.ThreeKey = Key(self, "3", "#", "3", "#")
        self.FourKey = Key(self, "4", "$", "4", "$")
        self.FiveKey = Key(self, "5", "%", "5", "%")
        self.SixKey = Key(self, "6", "^", "6", "^")
        self.SevenKey = Key(self, "7", "&&", "7", "&&")
        self.EightKey = Key(self, "8", "*", "8", "*")
        self.NineKey = Key(self, "9", "(", "9", "(")
        self.ZeroKey = Key(self, "0", ")", "0", ")")
        self.DashKey = Key(self, "-", "_", "-", "_")
        self.EqualsKey = Key(self, "=", "+", "=", "+")
        self.QKey = Key(self, "q", "Q", "q", "Q")
        self.WKey = Key(self, "w", "W", "w", "W")
        self.EKey = Key(self, "e", "E", "e", "E")
        self.RKey = Key(self, "r", "R", "r", "R")
        self.TKey = Key(self, "t", "T", "t", "T")
        self.YKey = Key(self, "y", "Y", "y", "Y")
        self.UKey = Key(self, "u", "U", "u", "U")
        self.IKey = Key(self, "i", "I", "i", "I")
        self.OKey = Key(self, "o", "O", "o", "O")
        self.PKey = Key(self, "p", "P", "p", "P")
        self.LSBracketKey = Key(self, "[", "{", "[", "{")
        self.RSBracketKey = Key(self, "]", "}", "]", "}")
        self.BackSlashKey = Key(self, "\\", "|", "\\", "|")
        self.AKey = Key(self, "a", "A", "a", "A")
        self.SKey = Key(self, "s", "S", "s", "S")
        self.DKey = Key(self, "d", "D", "d", "D")
        self.FKey = Key(self, "f", "F", "f", "F")
        self.GKey = Key(self, "g", "G", "g", "G")
        self.HKey = Key(self, "h", "H", "h", "H")
        self.JKey = Key(self, "j", "J", "j", "J")
        self.KKey = Key(self, "k", "K", "k", "k")
        self.LKey = Key(self, "l", "L", "l", "L")
        self.SemiColonKey = Key(self, ";", ":", ";", ":")
        self.ApostropheKey = Key(self, "'", "\"", "'", "\"")
        self.ZKey = Key(self, "z", "Z", "z", "Z")
        self.XKey = Key(self, "x", "X", "x", "X")
        self.CKey = Key(self, "c", "C", "c", "C")
        self.VKey = Key(self, "v", "V", "v", "V")
        self.BKey = Key(self, "b", "B", "b", "B")
        self.NKey = Key(self, "n", "N", "n", "N")
        self.MKey = Key(self, "m", "M", "m", "M")
        self.CommaKey = Key(self, ",", "<", ",", "<")
        self.PeriodKey = Key(self, ".", ">", ".", ">")
        self.SlashKey = Key(self, "/", "?", "/", "?")

        self.DeleteKey = Key(self, "", "", "delete", "delete", 110)
        self.ReturnKey = Key(self, "", "", "return", "return", 100)
        self.LeftShiftKey = Key(self, "", "", "shift", "shift", 150)
        self.RightShiftKey = Key(self, "", "", "shift", "shift", 130)
        self.CapsLockKey = Key(self, "", "", "caps", "caps", 120)
        self.SpaceKey = Key(self, " ", " ", "space", "space", 300)
        self.TabFill = Key(self, "", "", "", "", 100)

        self.LeftDoneKey = Key(self, "", "", "Done", "Done", 100)
        self.RightDoneKey = Key(self, "", "", "Done", "Done", 100)

        self.setAllNeighbors()

        self.firstRow = [
            self.TildaKey, self.OneKey, self.TwoKey, self.ThreeKey,
            self.FourKey, self.FiveKey, self.SixKey, self.SevenKey,
            self.EightKey, self.NineKey,
            self.ZeroKey, self.DashKey, self.EqualsKey, self.DeleteKey
            ]
        self.secondRow = [
            self.TabFill, self.QKey, self.WKey, self.EKey, self.RKey, self.TKey,
            self.YKey, self.UKey, self.IKey, self.OKey, self.PKey,
            self.LSBracketKey, self.RSBracketKey,
            self.BackSlashKey
            ]
        self.thirdRow = [
            self.CapsLockKey, self.AKey, self.SKey, self.DKey,
            self.FKey, self.GKey, self.HKey, self.JKey, self.KKey,
            self.LKey, self.SemiColonKey, self.ApostropheKey, self.ReturnKey
            ]
        self.fourthRow = [
            self.LeftShiftKey, self.ZKey, self.XKey, self.CKey, self.VKey,
            self.BKey, self.NKey, self.MKey, self.CommaKey,
            self.PeriodKey, self.SlashKey, self.RightShiftKey
            ]
        self.fifthRow = [self.LeftDoneKey, self.SpaceKey, self.RightDoneKey]

    def setAllNeighbors(self):
        self.TildaKey.setNeighbors(self.LeftDoneKey, self.QKey, self.DeleteKey, self.OneKey)
        self.OneKey.setNeighbors(self.LeftDoneKey, self.QKey, self.TildaKey, self.TwoKey)
        self.TwoKey.setNeighbors(self.LeftDoneKey, self.WKey, self.OneKey, self.ThreeKey)
        self.ThreeKey.setNeighbors(self.SpaceKey, self.EKey, self.TwoKey, self.FourKey)
        self.FourKey.setNeighbors(self.SpaceKey, self.RKey, self.ThreeKey, self.FiveKey)
        self.FiveKey.setNeighbors(self.SpaceKey, self.TKey, self.FourKey, self.SixKey)
        self.SixKey.setNeighbors(self.SpaceKey, self.YKey, self.FiveKey, self.SevenKey)
        self.SevenKey.setNeighbors(self.SpaceKey, self.UKey, self.SixKey, self.EightKey)
        self.EightKey.setNeighbors(self.SpaceKey, self.IKey, self.SevenKey, self.NineKey)
        self.NineKey.setNeighbors(self.SpaceKey, self.OKey, self.EightKey, self.ZeroKey)
        self.ZeroKey.setNeighbors(self.SpaceKey, self.PKey, self.NineKey, self.DashKey)
        self.DashKey.setNeighbors(self.RightDoneKey, self.LSBracketKey, self.ZeroKey, self.EqualsKey)
        self.EqualsKey.setNeighbors(self.RightDoneKey, self.RSBracketKey, self.DashKey, self.DeleteKey)
        self.QKey.setNeighbors(self.OneKey, self.AKey, self.BackSlashKey, self.WKey)
        self.WKey .setNeighbors(self.TwoKey, self.SKey, self.QKey, self.EKey)
        self.EKey.setNeighbors(self.ThreeKey, self.DKey, self.WKey, self.RKey)
        self.RKey.setNeighbors(self.FourKey, self.FKey, self.EKey, self.TKey)
        self.TKey.setNeighbors(self.FiveKey, self.GKey, self.RKey, self.YKey)
        self.YKey.setNeighbors(self.SixKey, self.HKey, self.TKey, self.UKey)
        self.UKey.setNeighbors(self.SevenKey, self.JKey, self.YKey, self.IKey)
        self.IKey.setNeighbors(self.EightKey, self.KKey, self.UKey, self.OKey)
        self.OKey.setNeighbors(self.NineKey, self.LKey, self.IKey, self.PKey)
        self.PKey.setNeighbors(self.ZeroKey, self.SemiColonKey, self.OKey, self.LSBracketKey)
        self.LSBracketKey.setNeighbors(self.DashKey, self.ApostropheKey, self.PKey, self.RSBracketKey)
        self.RSBracketKey.setNeighbors(self.EqualsKey, self.ReturnKey, self.LSBracketKey, self.BackSlashKey)
        self.BackSlashKey.setNeighbors(self.DeleteKey, self.ReturnKey, self.RSBracketKey, self.QKey)
        self.AKey.setNeighbors(self.QKey, self.ZKey, self.CapsLockKey, self.SKey)
        self.SKey.setNeighbors(self.WKey, self.XKey, self.AKey, self.DKey)
        self.DKey.setNeighbors(self.EKey, self.CKey, self.SKey, self.FKey)
        self.FKey.setNeighbors(self.RKey, self.VKey, self.DKey, self.GKey)
        self.GKey.setNeighbors(self.TKey, self.VKey, self.FKey, self.HKey)
        self.HKey.setNeighbors(self.YKey, self.NKey, self.GKey, self.JKey)
        self.JKey.setNeighbors(self.UKey, self.MKey, self.HKey, self.KKey)
        self.KKey.setNeighbors(self.IKey, self.CommaKey, self.JKey, self.LKey)
        self.LKey.setNeighbors(self.OKey, self.PeriodKey, self.KKey, self.SemiColonKey)
        self.SemiColonKey.setNeighbors(self.PKey, self.SlashKey, self.LKey, self.ApostropheKey)
        self.ApostropheKey.setNeighbors(self.LSBracketKey, self.RightShiftKey, self.SemiColonKey, self.ReturnKey)
        self.ZKey.setNeighbors(self.QKey, self.LeftDoneKey, self.LeftShiftKey, self.XKey)
        self.XKey.setNeighbors(self.SKey, self.SpaceKey, self.ZKey, self.CKey)
        self.CKey.setNeighbors(self.DKey, self.SpaceKey, self.XKey, self.VKey)
        self.VKey.setNeighbors(self.FKey, self.SpaceKey, self.CKey, self.BKey)
        self.BKey.setNeighbors(self.GKey, self.SpaceKey, self.VKey, self.NKey)
        self.NKey.setNeighbors(self.HKey, self.SpaceKey, self.BKey, self.MKey)
        self.MKey.setNeighbors(self.JKey, self.SpaceKey, self.NKey, self.CommaKey)
        self.CommaKey.setNeighbors(self.KKey, self.SpaceKey, self.MKey, self.PeriodKey)
        self.PeriodKey.setNeighbors(self.LKey, self.SpaceKey, self.CommaKey, self.SlashKey)
        self.SlashKey.setNeighbors(self.SemiColonKey, self.RightDoneKey, self.PeriodKey, self.RightShiftKey)

        self.DeleteKey.setNeighbors(self.RightDoneKey, self.BackSlashKey, self.EqualsKey, self.TildaKey)
        self.ReturnKey.setNeighbors(self.BackSlashKey, self.RightShiftKey, self.ApostropheKey, self.CapsLockKey)
        self.LeftShiftKey.setNeighbors(self.CapsLockKey, self.LeftDoneKey, self.RightShiftKey, self.ZKey)
        self.RightShiftKey.setNeighbors(self.ReturnKey, self.RightDoneKey, self.SlashKey, self.LeftShiftKey)
        self.CapsLockKey.setNeighbors(self.QKey, self.LeftShiftKey, self.ReturnKey, self.AKey)
        self.SpaceKey.setNeighbors(self.BKey, self.SevenKey, self.LeftDoneKey, self.RightDoneKey)

        self.LeftDoneKey.setNeighbors(self.LeftShiftKey, self.TildaKey, self.RightDoneKey, self.SpaceKey)
        self.RightDoneKey.setNeighbors(self.RightShiftKey, self.DeleteKey, self.SpaceKey, self.LeftDoneKey)

    def setRows(self):
        self.makeRow(0, 0, self.firstRow)
        self.makeRow(1, 0, self.secondRow)
        self.makeRow(2, 0, self.thirdRow)
        self.makeRow(3, 0, self.fourthRow)
        self.makeBottomRow(self.fifthRow)

    def makeRow(self, rowNumber, offset, row):
        xPosition = 5 + offset
        yPosition = (rowNumber * 55) + 5
        for key in row:
            key.move(xPosition, yPosition)
            xPosition += key.width

    def makeBottomRow(self, row):
        xPosition = 5
        yPosition = (4 * 55) + 5
        row[0].move(xPosition, yPosition)
        row[1].move(300, yPosition)
        row[2].move(785, yPosition)

    def changeCase(self):
        for key in self.firstRow:
            key.switchCase(self.currentCase)
        for key in self.secondRow:
            key.switchCase(self.currentCase)
        for key in self.thirdRow:
            key.switchCase(self.currentCase)
        for key in self.fourthRow:
            key.switchCase(self.currentCase)
        if self.currentCase == 0:
            self.currentCase = 1
        else:
            self.currentCase = 0

    def keyPressEvent(self, e):
        self.currentKey.unsetSelector()
        if e.key() == QtCore.Qt.Key_A:
            if self.currentKey.left is not None:
                self.currentKey = self.currentKey.left
        elif e.key() == QtCore.Qt.Key_D:
            if self.currentKey.right is not None:
                self.currentKey = self.currentKey.right
        elif e.key() == QtCore.Qt.Key_S:
            if self.currentKey.down is not None:
                self.currentKey = self.currentKey.down
        elif e.key() == QtCore.Qt.Key_W:
            if self.currentKey.up is not None:
                self.currentKey = self.currentKey.up
        elif e.key() == QtCore.Qt.Key_Return:
            if self.currentKey == self.CapsLockKey:
                self.changeCase()
            elif self.currentKey == self.LeftShiftKey or self.currentKey == self.RightShiftKey:
                self.shiftCheck = True
                self.changeCase()
            elif self.currentKey == self.LeftDoneKey or self.currentKey == self.RightDoneKey:
                if self.mainTextBox.text() == "":
                    self.mainTextBox.setText(self.originalText)
                self.close()
            elif self.currentKey == self.DeleteKey:
                self.mainText = self.mainText[:-1]
                self.mainTextBox.setText(self.mainText)
            else:
                inputKey = str(self.currentKey.lowerCaseValue)
                if self.currentCase == 1:
                    inputKey = str(self.currentKey.upperCaseValue)
                if self.isPassword:
                    self.mainText += "*"
                    self.passwordText += inputKey
                else:
                    self.mainText += inputKey
                if self.currentCase == 1 and self.shiftCheck:
                    self.changeCase()
                    self.shiftCheck = False
                self.mainTextBox.setText(self.mainText)
                if self.isPassword:
                    self.passwordObject.password = self.passwordText
        self.currentKey.setSelector()

class Key(QtGui.QPushButton):
    def __init__(self, window, lowerCaseValue, upperCaseValue, lowerCaseDisplay, upperCaseDisplay, width=60,
                 selected=False):
        super(Key, self).__init__(lowerCaseDisplay, window)
        self._currentCase = 0
        self._selected = selected
        self._width = width
        self._height = 55
        self.lowerCaseValue = lowerCaseValue
        self.upperCaseValue = upperCaseValue
        self.lowerCaseDisplay = lowerCaseDisplay
        self.upperCaseDisplay = upperCaseDisplay
        self.setText(lowerCaseDisplay)
        self.resize(self._width, self._height)
        self.setStyleSheet("font-weight: 500; background-color: rgba(21,29,38,255); color: rgba(251,204,81,255); "
                               "border: 3px solid rgba(26,75,71,255); outline: none;")
        if self._selected:
            self.setSelector()
        self._up = None
        self._down = None
        self._left = None
        self._right = None

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def up(self):
        return self._up

    @property
    def down(self):
        return self._down

    @property
    def left(self):
        return self._left

    @property
    def right(self):
        return self._right

    @property
    def selected(self):
        return self._selected

    def setNeighbors(self, up, down, left, right):
        self._up = up
        self._down = down
        self._left = left
        self._right = right

    def switchCase(self, currentCase):
        if currentCase == 0:
            self.setText(self.upperCaseDisplay)
        else:
            self.setText(self.lowerCaseDisplay)

    def setSelector(self):
        self.setStyleSheet("font-weight: 500; background-color: rgba(21,29,38,255); color: rgba(251,204,81,255); "
                          "border: 3px solid rgba(251,204,81,255); outline: none;")

    def unsetSelector(self):
        self.setStyleSheet("font-weight: 500; background-color: rgba(21,29,38,255); color: rgba(251,204,81,255); "
                           "border: 3px solid rgba(26,75,71,255); outline: none;")