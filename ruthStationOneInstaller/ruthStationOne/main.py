#!/usr/bin/python
import sys
import LauncherWindow
from PyQt4 import QtGui


def main():
    app = QtGui.QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    width, height = screen_resolution.width(), screen_resolution.height()
    GUI = LauncherWindow.LauncherWindow(app, width, height)
    #GUI.displayWelcome()
    GUI.setUpMenu()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
