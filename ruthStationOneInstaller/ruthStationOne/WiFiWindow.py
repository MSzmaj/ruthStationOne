#!/usr/bin/python
import time
from PyQt4 import QtGui, QtCore
import Keyboard
import passwordStorage
import WiFi

class WiFiWindow(QtGui.QMainWindow):
    def __init__(self, app, width, height, name):
        super(WiFiWindow, self).__init__()
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.name = name
        self.defaultValues = ["Enter WiFi password"]
        self.ssidDictionary = {}
        self.ssids = ["Community", "Test1", "Test2", "Test3", "Test4", "Test5", "Test6", "Test7", "Test8", "Test9"]
        #networks = WiFi.getWifiNetworks()
        #self.ssids = []
        #for network in networks:
        #    self.ssids.append(network.ssid)
        self.keyboardGUI = None
        self.windowWidth = width
        self.windowHeight = height
        self.password = passwordStorage.passwordStorage()
        self.selector = QtGui.QPushButton("", self)
        self.selectorPositionHorizontal = 47
        self.selectorPositionVertical = 121
        self.selectorStateVerticalLeft = 0
        self.selectorStateVerticalRight = 0
        self.selectorStateHorizontal = 0
        self.selectorYMovement = 50
        self.selectorXMovement = 250
        self.selectorVerticalMaxLeft = len(self.ssids)
        self.selectorVerticalMaxRight = 1
        self.selectorHorizontalMax = 1
        self.setStyleSheet("background-color: RGBA(253, 142, 175, 255);")
        self.app = app
        screen_resolution = app.desktop().screenGeometry()
        screenWidth, screenHeight = screen_resolution.width(), screen_resolution.height()
        self.setGeometry((screenWidth / 2) - (self.windowWidth / 2), (screenHeight / 2) - (self.windowHeight / 2.5), self.windowWidth, self.windowHeight)
        self.setWindowTitle(name.lower() + "StationOne")
        self.startUp()
        self.createSSIDList(self.ssids)
        self.show()

    def startUp(self):
        self.setupLabel = QtGui.QPushButton("WiFi Setup", self)
        self.setupLabel.resize(300, 60)
        self.setupLabel.setStyleSheet(
            "background-color: rgba(255,255,255,0); color: rgba(255,255,255,255); font-size:50px")
        self.setupLabel.move(250, 10)

        self.passTextBox = QtGui.QPushButton(self.defaultValues[0], self)
        self.passTextBox.resize(200, 40)
        self.passTextBox.setStyleSheet("background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black")
        self.passTextBox.move(300, 125)

        self.continueButton = QtGui.QPushButton("Finish", self)
        self.continueButton.resize(200, 40)
        self.continueButton.setStyleSheet(
            "background-color: rgba(21, 29, 38, 255); color: rgba(255,255,255,255); border-radius: 5px;")
        self.continueButton.move(300, 175)

        self.selector.setStyleSheet("background-color: rgba(0,0,0,0); border: 5px solid RGBA(156, 254, 204, 255); border-radius: 5px;")
        self.selector.resize(207.5, 48.5)
        self.selector.move(self.selectorPositionHorizontal, self.selectorPositionVertical)

    def createSSIDList(self, ssids):
        value = 0
        yPosition = 125
        for ssid in ssids:
            self.ssidDictionary[value] = QtGui.QPushButton(ssid, self)
            self.ssidDictionary[value].resize(200, 40)
            if value > 1:
                self.ssidDictionary[value].setStyleSheet("background-color: rgba(255,255,255," + str(100) + "); color: rgba(0,0,0," + str(100) + "); border-radius: 5px;")
            else:
                self.ssidDictionary[value].setStyleSheet("background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black")
            self.ssidDictionary[value].move(50, yPosition)
            value += 1
            yPosition += 50

    def displayWelcome(self):
        self.app.processEvents()
        self.fadeIn(self.welcomeLabel)
        self.fadeOut(self.welcomeLabel)
        self.welcomeLabel.setText("ruthStationOne")
        time.sleep(2)
        self.fadeIn(self.welcomeLabel)
        self.fadeOut(self.welcomeLabel)

    def setUpMenu(self):
        #Configure menu buttons
        self.fadeOutHidingLabel(self.hidingLabel)

    def fadeIn(self, item):
        self.completed = 0
        while self.completed < 255:
            self.completed += 5
            item.setStyleSheet("font-size: 50px; background-color: rgba(0,0,0,0); color: rgba(0,0,0," + str(self.completed) + ");")
            time.sleep(0.01)
            self.app.processEvents()

    def fadeOut(self, item):
        self.completed = 260
        while self.completed > 0:
            self.completed -= 5
            item.setStyleSheet("font-size: 50px; background-color: rgba(0,0,0,0); color: rgba(0,0,0," + str(self.completed) + ");")
            time.sleep(0.01)
            self.app.processEvents()

    def fadeOutHidingLabel(self, item):
        self.completed = 260
        while self.completed > 0:
            self.completed -= 5
            item.setStyleSheet("background-color: rgba(255,255,255," + str(self.completed) + "); color: rgba(0,0,0,0);")
            time.sleep(0.01)
            self.app.processEvents()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_S:
            if self.selectorStateHorizontal == 1 and self.selectorStateVerticalRight == 0:
                self.selectorPositionVertical += self.selectorYMovement
                self.selector.move(self.selectorPositionHorizontal, self.selectorPositionVertical)
                self.selectorStateVerticalRight += 1
            if self.selectorStateHorizontal == 0 and self.selectorStateVerticalLeft < self.selectorVerticalMaxLeft - 1:
                for key, value in self.ssidDictionary.items():
                    newYValue = value.y() - self.selectorYMovement
                    value.move(50, newYValue)
                    if newYValue < 70 or newYValue > 200:
                        value.setStyleSheet("background-color: rgba(255,255,255," + str(100) + "); color: rgba(0,0,0," + str(100)  + "); border-radius: 5px")
                    else:
                        value.setStyleSheet("background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black")
                self.selectorStateVerticalLeft += 1
        elif e.key() == QtCore.Qt.Key_W:
            if self.selectorStateHorizontal == 1 and self.selectorStateVerticalRight == 1:
                self.selectorPositionVertical -= self.selectorYMovement
                self.selector.move(self.selectorPositionHorizontal, self.selectorPositionVertical)
                self.selectorStateVerticalRight -= 1
            if self.selectorStateHorizontal == 0 and self.selectorStateVerticalLeft > 0:
                for key, value in self.ssidDictionary.items():
                    newYValue = value.y() + self.selectorYMovement
                    value.move(50, newYValue)
                    if newYValue < 70 or newYValue > 200:
                        value.setStyleSheet("background-color: rgba(255,255,255," + str(100)  + "); color: rgba(0,0,0," + str(100)  + "); border-radius: 5px")
                    else:
                        value.setStyleSheet("background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black")
                self.selectorStateVerticalLeft -= 1
        elif e.key() == QtCore.Qt.Key_D:
            if self.selectorStateHorizontal == 0:
                self.selectorPositionHorizontal += self.selectorXMovement
                self.selector.move(self.selectorPositionHorizontal, 121)
                self.selectorStateHorizontal += 1
        elif e.key() == QtCore.Qt.Key_A:
            if self.selectorStateHorizontal == 1:
                self.selectorPositionHorizontal -= self.selectorXMovement
                self.selector.move(self.selectorPositionHorizontal, 121)
                self.selectorStateHorizontal -= 1
            self.selectorStateHorizontal = 0
            self.selectorStateVerticalRight = 0
            self.selectorPositionVertical = 121
        elif e.key() == QtCore.Qt.Key_Return:
            if self.selectorStateHorizontal == 1 and self.selectorStateVerticalRight == 0:
                self.keyBoardGUI = Keyboard.Keyboard(self.app, self.passTextBox, True, self.password, self.defaultValues)
            elif self.selectorStateVerticalRight == 1:
                print("SSID: " + self.ssids[self.selectorStateVerticalLeft] + ", PASSWORD: " + self.password.password)
                #WiFi.setWifiNetwork(self.ssids[self.selectorStateVerticalLeft], self.password.password)
                self.close()
