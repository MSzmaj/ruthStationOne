#!/usr/bin/python
import os
import getpass
from PyQt4 import QtGui, QtCore
import FileOperations


class FileTransferWindow(QtGui.QMainWindow):
    def __init__(self, app, width, height, parent=None):
        super(FileTransferWindow, self).__init__(parent)
        self.app = app
        self.x = 100
        self.y = 100
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.styles = Styles()
        self.visuals = Icons(self.app)
        self.buttonPosition = 0
        self.windowWidth = width
        self.windowHeight = height
        self.gameLocation = GameLocation()
        self.path = "/media/" + getpass.getuser() + "/"
        self.fileView = FileView(app, self, 15, 60, self.windowWidth - 30, self.windowHeight - 120)
        self.getDirectoryContents()
        self.keyboardGUI = None
        self.setGeometry(100, 100, self.windowWidth, self.windowHeight)
        self.test = QtGui.QWidget()
        self.fileView.displayContents(self.fileViewContents, self.directoryContents)
        self.setUpWindow()
        self.setUpButtons()
        self.show()
        self.warnings = Warnings(self.app, 500, 325, self.x, self.y + 175)
        self.console = None

    def getDirectoryContents(self):
        self.directoryContents = FileOperations.getListOfDirectory(self.path)
        self.directoryContents = [x for x in self.directoryContents if not x.startswith(".")]
        self.fileViewContents = []
        maxFileViewLength = len(self.directoryContents) if len(self.directoryContents) <= 14 else 14
        for x in range(0, maxFileViewLength):
            self.fileViewContents.append(x)

    def setUpWindow(self):
        self.directoryLabel = QtGui.QPushButton(getpass.getuser(), self)
        self.directoryLabel.resize(self.windowWidth, 40)
        self.directoryLabel.setStyleSheet("Text-Align: left; background-color: rgba(255,255,255,0); color: rgba(255,255,255,255); font-size:30px")
        self.directoryLabel.move(20, 10)
        self.setStyleSheet("background-color: rgba(208, 207, 208, 255)")

    def setUpButtons(self):
        styles = Styles()
        height = 648

        self.helpButton = QtGui.QPushButton("Help", self)
        self.helpButton.resize(90, 45)
        self.helpButton.setStyleSheet(styles.buttonStyle)
        self.helpButton.move(400, 10)

        self.importButton = QtGui.QPushButton("Import", self)
        self.importButton.resize(90, 45)
        self.importButton.setStyleSheet(styles.buttonStyle)
        self.importButton.move(20, height)

        self.selectAllButton = QtGui.QPushButton("Select All", self)
        self.selectAllButton.resize(90, 45)
        self.selectAllButton.setStyleSheet(styles.buttonStyle)
        self.selectAllButton.move(120, height)

        self.deselectAllButton = QtGui.QPushButton("Deselect All", self)
        self.deselectAllButton.resize(90, 45)
        self.deselectAllButton.setStyleSheet(styles.buttonStyle)
        self.deselectAllButton.move(220, height)

        self.doneButton = QtGui.QPushButton("Done", self)
        self.doneButton.resize(70, 45)
        self.doneButton.setStyleSheet(styles.buttonStyle)
        self.doneButton.move(320, height)

        self.refreshButton = QtGui.QPushButton("Refresh", self)
        self.refreshButton.resize(80, 45)
        self.refreshButton.setStyleSheet(styles.buttonStyle)
        self.refreshButton.move(400, height)

        self.topButtoms = [self.helpButton]
        self.bottomButtons = [self.importButton, self.selectAllButton, self.deselectAllButton, self.doneButton, self.refreshButton]

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_S:
            if self.fileView.selectorArea == 0:
                self.fileView.moveSelector(1, len(self.directoryContents), self.fileViewContents, self.directoryContents)
            else:
                self.fileView.selectorArea = 0
                self.fileView.dataObjects[self.fileView.selectorPosition].setHighlightedBackground()
                self.importButton.setStyleSheet(self.styles.buttonStyle)
                self.deselectButtons()
        elif e.key() == QtCore.Qt.Key_W:
            if self.fileView.selectorArea == 0:
                self.fileView.moveSelector(-1, len(self.directoryContents), self.fileViewContents, self.directoryContents)
            else:
                self.fileView.selectorArea = 0
                self.fileView.dataObjects[self.fileView.selectorPosition].setHighlightedBackground()
                self.deselectButtons()
        elif e.key() == QtCore.Qt.Key_D:
            if self.fileView.selectorArea == 0:
                self.fileView.selectorArea = 1
                self.fileView.dataObjects[self.fileView.selectorPosition].setSemiBackground()
                self.importButton.setStyleSheet(self.styles.highlightedButtonStyle)
            elif self.fileView.selectorArea == 1 and self.buttonPosition < len(self.bottomButtons) - 1:
                self.bottomButtons[self.buttonPosition].setStyleSheet(self.styles.buttonStyle)
                self.buttonPosition += 1
                self.bottomButtons[self.buttonPosition].setStyleSheet(self.styles.highlightedButtonStyle)
        elif e.key() == QtCore.Qt.Key_A:
            if self.fileView.selectorArea == 0:
                self.fileView.selectorArea = 2
                self.fileView.dataObjects[self.fileView.selectorPosition].setSemiBackground()
                self.helpButton.setStyleSheet(self.styles.highlightedButtonStyle)
            if self.fileView.selectorArea == 1 and self.buttonPosition > 0:
                self.bottomButtons[self.buttonPosition].setStyleSheet(self.styles.buttonStyle)
                self.buttonPosition -= 1
                self.bottomButtons[self.buttonPosition].setStyleSheet(self.styles.highlightedButtonStyle)
        elif e.key() == QtCore.Qt.Key_Return:
            if self.fileView.selectorArea == 0:
                path = self.fileView.selectObject(self.directoryContents)
                if path is not None:
                    if "..." in path:
                        path = path.rsplit('/', 3)[0] + '/'
                    self.path = path
                    self.getDirectoryContents()
                    self.fileView.path = self.path
                    self.fileView.displayContents(self.fileViewContents, self.directoryContents)
                    path = path.rsplit('/', 3)
                    path = path[len(path) - 2]
                    self.directoryLabel.setText(path)
            elif self.fileView.selectorArea == 1:
                if self.buttonPosition == 0:
                    if len(self.fileView.selectedFiles) == 0:
                        self.warnings = Warnings(self.app, 500, 300, self.x, self.y + 175)
                        self.warnings.changeText()
                    else:
                        self.console = Console(self.app, 300, 200, self.x, self.y, self.gameLocation, self)
                elif self.buttonPosition == 1:
                    self.fileView.selectAllFiles()
                elif self.buttonPosition == 2:
                    self.fileView.deselectAllFiles()
                elif self.buttonPosition == 3:
                    self.close()
                else:
                    self.fileView.resetFileView()
                    self.getDirectoryContents()
                    self.fileView.displayContents(self.fileViewContents, self.directoryContents)
                    self.buttonPosition = 0
                    self.refreshButton.setStyleSheet(self.styles.buttonStyle)
            elif self.fileView.selectorArea == 2:
                self.warnings = Warnings(self.app, 500, 325, self.x, self.y + 175)

    def deselectButtons(self):
        self.buttonPosition = 0
        self.importButton.setStyleSheet(self.styles.buttonStyle)
        self.selectAllButton.setStyleSheet(self.styles.buttonStyle)
        self.deselectAllButton.setStyleSheet(self.styles.buttonStyle)
        self.doneButton.setStyleSheet(self.styles.buttonStyle)
        self.refreshButton.setStyleSheet(self.styles.buttonStyle)
        self.helpButton.setStyleSheet(self.styles.buttonStyle)

    def importFiles(self):
        if '' in self.fileView.selectedFiles:
            self.fileView.selectedFiles = [x for x in self.fileView.selectedFiles if x != '']
        FileOperations.moveFile(self.path, self.fileView.selectedFiles, self.gameLocation)


class Warnings(QtGui.QMainWindow):
    _instructionsText = "Instructions: Select all the games you want to \nimport for a single game system (e.g. N64) at a \ntime and then select 'Import'. Once imported \nthe games will be grayed out and you can \nimport more for another system. \n\n Only games not in your library \n will not be grayed out."
    _noItemsSelectedText = "Please select at least one item."
    def __init__(self, app, width, height, x, y, parent=None):
        super(Warnings, self).__init__(parent)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.app = app
        self.currentText = 0
        self.styles = Styles()
        self.windowWidth = width
        self.setGeometry(x, y, width, height)
        self.setUpWindow()
        self.show()

    def setUpWindow(self):
        self.warningLabel = QtGui.QToolButton(self)
        self.warningLabel.setText(self._instructionsText)
        self.warningLabel.resize(self.windowWidth - 50, 250)
        self.warningLabel.setStyleSheet("Text-Align: left; background-color: rgba(255,255,255,0); color: rgba(255,255,255,255); font-size:20px; outline: none;")
        self.warningLabel.move(20, 10)
        self.setStyleSheet("background-color: rgba(9, 33, 46, 255);")

        self.doneButton = QtGui.QPushButton("Done", self)
        self.doneButton.resize(70, 45)
        self.doneButton.setStyleSheet("background-color: rgba(0,0,0,255); color: rgba(255,255,255,255); font-size:20px; border: 3px solid white; border-radius: 5px; outline: none;")
        self.doneButton.move((self.width() / 2) - (self.doneButton.width() / 2), self.height() - 60)

    def changeText(self):
        if self.currentText == 0:
            self.currentText = 1
            self.warningLabel.setText(self._noItemsSelectedText)
        else:
            self.currentText = 0
            self.warningLabel.setText(self._instructionsText)

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Return:
            self.close()


class Console(QtGui.QMainWindow):
    def __init__(self, app, width, height, x, y, gameLocation, parent):
        super(Console, self).__init__(parent)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.gameLocation = gameLocation
        self.systemDictionary = {}
        self.selectorMax = 0
        self.selectorYMovement = 50
        self.systemList = ["N64", "PS1", "GBA", "GB", "PS2", "SNES", "NES", "SEGA GENESIS"]
        self.app = app
        self.parent = parent
        self.selectorPosition = 0
        self.styles = Styles()
        self.windowWidth = width
        self.setGeometry(x, y, width, height)
        self.setUpWindow(self.systemList)
        self.setSelector()
        self.show()
        self.setFocus()

    def setSelector(self):
        self.selector = QtGui.QPushButton("", self)
        self.selector.setStyleSheet("background-color: rgba(0,0,0,0); border: 5px solid rgba(17, 108, 214, 255); border-radius: 5px; outline: none;")
        self.selector.resize(200, 42.5)
        self.selector.move(50, 50)

    def setUpWindow(self, systems):
        self.selectorMax = len(systems) - 1
        value = 0
        yPosition = 50
        for system in systems:
            self.systemDictionary[value] = QtGui.QPushButton(system, self)
            self.systemDictionary[value].resize(200, 40)
            if value > 1:
                self.systemDictionary[value].setStyleSheet(
                    "background-color: rgba(255,255,255," + str(100) + "); color: rgba(0,0,0," + str(
                        100) + "); border-radius: 5px;")
            else:
                self.systemDictionary[value].setStyleSheet(
                    "background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black; outline: none;")
            self.systemDictionary[value].move(50, yPosition)
            value += 1
            yPosition += 50

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Return:
            self.gameLocation.location = str(self.systemDictionary[self.selectorPosition].text())
            self.parent.importFiles()
            self.close()
        elif e.key() == QtCore.Qt.Key_S:
            if self.selectorPosition < self.selectorMax:
                for key, value in self.systemDictionary.items():
                    newYValue = value.y() - self.selectorYMovement
                    value.move(50, newYValue)
                    if newYValue < 25 or newYValue > 100:
                        value.setStyleSheet(
                            "background-color: rgba(255,255,255," + str(100) + "); color: rgba(0,0,0," + str(
                                100) + "); border-radius: 5px")
                    else:
                        value.setStyleSheet(
                            "background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black")
                self.selectorPosition += 1
        elif e.key() == QtCore.Qt.Key_W:
            if self.selectorPosition > 0:
                for key, value in self.systemDictionary.items():
                    newYValue = value.y() + self.selectorYMovement
                    value.move(50, newYValue)
                    if newYValue < 25 or newYValue > 100:
                        value.setStyleSheet(
                            "background-color: rgba(255,255,255," + str(100) + "); color: rgba(0,0,0," + str(
                                100) + "); border-radius: 5px")
                    else:
                        value.setStyleSheet(
                            "background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black")
                self.selectorPosition -= 1


class FileView(QtGui.QPushButton):
    def __init__(self, app, window, x, y, width, height):
        super(FileView, self).__init__(window)
        self.app = app
        self.gamePath = '/home/michalszmaj/RETROPIE/'
        self.width = width
        self.height = height
        self.window = window
        self.path = window.path
        self.selectedFiles = []
        self.selectorPosition = 0
        self.selectorArea = 0
        self.dataObjects = None
        self.setStyleSheet("background-color: rgba(255,255,255,255); color: rgba(255,255,255,255); border: none; outline: none;")
        self.resize(self.width, self.height)
        self.move(x, y)
        self.setUpDataObjects()

    def setUpDataObjects(self):
        self.dataObjects = []
        yPosition = self.y() + 10
        position = 0
        for x in range(0, 500):
            self.dataObjects.append(DataObject(self.app, position, False, self.window, False, True))
            self.dataObjects[position].setUpObject(25, yPosition, 40, self.width - 15, "")
            yPosition += 40
            position += 1

    def resetFileView(self):
        self.selectedFiles = []
        self.selectorPosition = 0
        self.selectorArea = 0
        yPosition = self.y() + 10
        for item in self.dataObjects:
            item.reset(25, yPosition)
            yPosition += 40

    def displayContents(self, contentsToDisplay, contents):
        self.resetFileView()
        yPosition = self.y() + 10
        position = 0
        visible = True
        if self.path != '/':
            contents.insert(0, "...")
            if len(contentsToDisplay) < 13:
                if len(contentsToDisplay) == 0:
                    contentsToDisplay.append(0)
                else:
                    contentsToDisplay.append(contentsToDisplay[len(contentsToDisplay) - 1] + 1)
        for _ in contents:
            if position not in contentsToDisplay:
                visible = False
            isFile = os.path.isfile(self.path + contents[position])
            exists = FileOperations.checkIfGameExists(contents[position], self.gamePath)
            self.dataObjects[position].isFile = isFile
            self.dataObjects[position].selectable = False if not isFile or exists else True
            self.dataObjects[position].exists = exists
            self.dataObjects[position].visible = visible
            self.dataObjects[position].setUpObject(25, yPosition, 40, self.width - 15, contents[position])
            yPosition += 40
            position += 1
        self.dataObjects[0].setHighlightedBackground()

    def moveSelector(self, direction, maximum, contentsToDisplay, contents):
        newPosition = self.selectorPosition + direction
        if newPosition not in contentsToDisplay:
            if newPosition < contentsToDisplay[0]:
                self.scrollFileView(-1, contentsToDisplay, contents)
            elif newPosition > contentsToDisplay[len(contentsToDisplay) - 1]:
                self.scrollFileView(1, contentsToDisplay, contents)
        if 0 <= newPosition < maximum:
            if not self.dataObjects[self.selectorPosition].selected:
                self.dataObjects[self.selectorPosition].setDefaultBackground()
            elif self.dataObjects[self.selectorPosition].selected:
                self.dataObjects[self.selectorPosition].select()
            self.selectorPosition = newPosition
            self.dataObjects[self.selectorPosition].setHighlightedBackground()

    def selectObject(self, contents):
        if self.dataObjects[self.selectorPosition].selected == 0 and self.dataObjects[self.selectorPosition].selectable:
            self.dataObjects[self.selectorPosition].select()
            self.selectedFiles.append(contents[self.selectorPosition])
        elif self.dataObjects[self.selectorPosition].selected == 1 and self.dataObjects[self.selectorPosition].selectable:
            self.dataObjects[self.selectorPosition].deselect()
            self.selectedFiles.remove(contents[self.selectorPosition])
        elif not self.dataObjects[self.selectorPosition].isFile and not self.dataObjects[self.selectorPosition].selectable:
            return self.path + contents[self.selectorPosition] + '/'

    def scrollFileView(self, direction, contentsToDisplay, contents):
        newPosition = self.selectorPosition + direction
        if 0 <= newPosition < len(contents):
            if newPosition < contentsToDisplay[0]:
                self.dataObjects[contentsToDisplay[len(contentsToDisplay) - 1]].hideObject()
                self.dataObjects[newPosition].showObject()
                contentsToDisplay.pop()
                contentsToDisplay.insert(0, newPosition)
                for dataObject in self.dataObjects:
                    dataObject.moveDataObject(-1)
            else:
                self.dataObjects[contentsToDisplay[0]].hideObject()
                self.dataObjects[newPosition].showObject()
                contentsToDisplay.pop(0)
                contentsToDisplay.append(newPosition)
                for dataObject in self.dataObjects:
                    dataObject.moveDataObject(1)

    def selectAllFiles(self):
        for file in self.dataObjects:
            if file.isFile and file.selectable:
                self.selectedFiles.append(str(file.titleBox.text()))
                file.selected = 1
                if file.visible:
                    file.select()

    def deselectAllFiles(self):
        for file in self.dataObjects:
            file.deselectNonHighlight()
            self.selectedFiles = []
        self.dataObjects[self.selectorPosition].setSemiBackground()


class DataObject:
    def __init__(self, app, position, isFile, parent, visible, exists):
        self.app = app
        self.selectable = False
        self.styles = Styles()
        self.isFile = isFile
        self.exists = exists
        self.icons = Icons(self.app)
        self.title = ""
        self.icon = self.icons.blankIcon
        self.checkBox = QtGui.QPushButton("", parent)
        self.iconBox = QtGui.QPushButton("", parent)
        self.titleBox = QtGui.QPushButton("", parent)
        self.currentStyle = self.styles.style
        self.position = position
        self.selected = False
        self.visible = visible

    def moveDataObject(self, direction):
        vector = direction * -1 * 40
        self.checkBox.move(self.checkBox.x(), self.checkBox.y() + vector)
        self.iconBox.move(self.iconBox.x(), self.iconBox.y() + vector)
        self.titleBox.move(self.titleBox.x(), self.titleBox.y() + vector)

    def setRowColor(self):
        if self.visible:
            self.currentStyle = self.styles.style if self.position % 2 == 0 else self.styles.altStyle
            if self.exists:
                self.currentStyle = self.styles.grayedOut
        else:
            self.currentStyle = self.styles.invisibleStyle

    def setObjectIcon(self):
        if self.visible:
            self.icon = self.icons.fileIcon if self.isFile else self.icons.directoryIcon
            if self.exists:
                self.icon = self.icons.existsIcon
        else:
            self.icon = self.icons.blankIcon
        self.iconBox.setIcon(QtGui.QIcon(self.icon))

    def setUpObject(self, x, y, height, totalWidth, title):
        self.checkBox.resize(30, height)
        self.iconBox.resize(90, height)
        self.titleBox.resize(totalWidth - 125, height)
        self.setRowColor()
        self.checkBox.setStyleSheet(self.currentStyle)
        self.iconBox.setStyleSheet(self.currentStyle)
        self.titleBox.setStyleSheet(self.styles.leftAlign + self.currentStyle)
        self.titleBox.setText(title)
        self.setObjectIcon()
        self.checkBox.move(x, y)
        self.iconBox.move(x + 30, y)
        self.titleBox.move(x + 120, y)

    def select(self):
        self.selected = 1
        self.checkBox.setStyleSheet(self.styles.importStyle)
        self.iconBox.setStyleSheet(self.styles.importStyle)
        self.titleBox.setStyleSheet(self.styles.leftAlign + self.styles.importStyle)

    def deselectNonHighlight(self):
        self.selected = 0
        self.setRowColor()
        self.checkBox.setStyleSheet(self.currentStyle)
        self.iconBox.setStyleSheet(self.currentStyle)
        self.titleBox.setStyleSheet(self.styles.leftAlign + self.currentStyle)

    def deselect(self):
        self.selected = 0
        self.setRowColor()
        self.checkBox.setStyleSheet(self.currentStyle)
        self.iconBox.setStyleSheet(self.styles.selectedStyle)
        self.titleBox.setStyleSheet(self.styles.leftAlign + self.styles.selectedStyle)

    def setDefaultBackground(self):
        self.setRowColor()
        self.checkBox.setStyleSheet(self.currentStyle)
        self.iconBox.setStyleSheet(self.currentStyle)
        self.titleBox.setStyleSheet(self.styles.leftAlign + self.currentStyle)
        self.setObjectIcon()
        self.iconBox.setIcon(QtGui.QIcon(self.icon))

    def setSemiBackground(self):
        self.iconBox.setStyleSheet(self.styles.semiSelected)
        self.titleBox.setStyleSheet(self.styles.leftAlign + self.styles.semiSelected)

    def setHighlightedBackground(self):
        self.iconBox.setStyleSheet(self.styles.selectedStyle)
        self.titleBox.setStyleSheet(self.styles.leftAlign + self.styles.selectedStyle)
        self.setObjectIcon()
        self.iconBox.setIcon(QtGui.QIcon(self.icon))

    def hideObject(self):
        self.visible = False
        self.setRowColor()
        self.checkBox.setStyleSheet(self.currentStyle)
        self.iconBox.setStyleSheet(self.currentStyle)
        self.titleBox.setStyleSheet(self.currentStyle)
        self.setObjectIcon()
        self.iconBox.setIcon(QtGui.QIcon(self.icon))

    def showObject(self):
        self.visible = True
        self.setRowColor()
        self.checkBox.setStyleSheet(self.currentStyle)
        self.iconBox.setStyleSheet(self.currentStyle)
        self.titleBox.setStyleSheet(self.currentStyle)
        self.setObjectIcon()
        self.iconBox.setIcon(QtGui.QIcon(self.icon))

    def reset(self, x, y):
        self.selected = False
        self.visible = False
        self.setRowColor()
        self.checkBox.setStyleSheet(self.currentStyle)
        self.iconBox.setStyleSheet(self.currentStyle)
        self.titleBox.setStyleSheet(self.currentStyle)
        self.setObjectIcon()
        self.iconBox.setIcon(QtGui.QIcon(self.icon))
        self.titleBox.setText("")
        self.checkBox.move(x, y)
        self.iconBox.move(x + 30, y)
        self.titleBox.move(x + 120, y)


class Styles:
    def __init__(self):
        self.leftAlign = "Text-Align: left;"
        self.style = "background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border: none; outline: none;"
        self.altStyle = "background-color: rgba(208, 207, 208, 255); color: rgba(0,0,0,255); border: none; outline: none;"
        self.invisibleStyle = "background-color: rgba(0,0,0,0); color: rgba(0,0,0,0); border: none; outline: none;"
        self.importStyle = "background-color: rgba(68, 60, 119, 255); color: rgba(255,255,255,255); border: none; outline: none;"
        self.selectedStyle = "background-color: rgba(17, 108, 214, 255); color: rgba(255,255,255,255); border: none; outline: none;"
        self.buttonStyle = "background-color: rgba(21, 29, 38, 255); color: rgba(255,255,255,255); border-radius: 5px; outline: none;"
        self.highlightedButtonStyle = "background-color: rgba(17, 108, 214, 255); color: rgba(255,255,255,255); border-radius: 5px; outline: none;"
        self.semiSelected = "background-color: rgba(27, 60, 78, 150); color: rgba(0,0,0,255); border: none; outline: none;"
        self.grayedOut = "background-color: rgba(60, 63, 65, 150); color: rgba(255,255,255,255); border: none; outline: none;"


class Icons:
    def __init__(self, app):
        self.app = app
        self.fileIcon = QtGui.QPixmap('Icons/file.png')
        self.directoryIcon = QtGui.QPixmap('Icons/directory.png')
        self.blankIcon = QtGui.QPixmap('Icons/blank.png')
        self.existsIcon = QtGui.QPixmap('Icons/fileExists.png')


class GameLocation:
    def __init__(self):
        self._location = ""

    @property
    def location(self):
        return self._location

    @location.setter
    def location(self, value):
        self._location = value