#!/usr/bin/python
import subprocess
import imp
import WiFiWindow
import FileTransferWindow

def dispatch(self, app, horizontal, vertical):
    if vertical == 0:
        if horizontal == 0:
            runEmulationStation()
        elif horizontal == 1:
            runKodi()
        elif horizontal == 2:
            runMusic()
    else:
        if horizontal == 0:
            power("shutdown -h now")
        elif horizontal == 1:
            power("reboot")
        elif horizontal == 2:
            wifi(self, app)
        elif horizontal == 3:
            importFiles(self, app)


def power(option):
    process = subprocess.Popen(option.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()


def wifi(self, app):
    self.wifi = WiFiWindow.WiFiWindow(app, 550, 300, "")


def importFiles(self, app):
    self.fileTransfer = FileTransferWindow.FileTransferWindow(app, 500, 700)


def runKodi():
    print("KODI")


def runEmulationStation():
    print("GAME")


def runMusic():
    print("MUSIC")
