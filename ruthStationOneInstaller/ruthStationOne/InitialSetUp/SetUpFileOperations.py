#!/usr/bin/python
import os

def setUpSettingsFile():
    settingsFile = open(os.path.join(os.path.expanduser('~'), 'Documents', 'configFiles/config'), 'w')
    settingsFile.truncate()
    settingsFile.write("NAME=\n")
    settingsFile.write("WIFI=\n")
    settingsFile.write("WIFIPASS=\n")

def setUpBootFile():
    bootFile = open(os.path.join(os.path.expanduser('~'), 'Documents', 'configFiles/boot'), 'w')
    bootFile.truncate()
    bootFile.write("boot=launcher\n")

def writeToSettingsFile(textPath, text):
    if not os.path.isfile(os.path.join(os.path.expanduser('~'), 'Documents', 'configFiles/config')):
        setUpSettingsFile()
    settingsFileR = open(os.path.join(os.path.expanduser('~'), 'Documents', 'configFiles/config'), 'r')
    data = settingsFileR.read()
    settingsFileR.close()
    index = data.index(textPath + "=") + len(textPath) + 1

    data = data[:index] + text + data[index:]

    settingsFileW = open(os.path.join(os.path.expanduser('~'), 'Documents', 'configFiles/config'), 'w')
    settingsFileW.write(data)
    settingsFileW.close()

def alterBootFile(app):
    settingsFileW = open(os.path.join(os.path.expanduser('~'), 'Documents', 'configFiles/boot'), 'w')
    data = "boot=" + app
    settingsFileW.write(data)
    settingsFileW.close()