#!/usr/bin/python
import sys
import SetUpWindow
import SetUpFileOperations
from PyQt4 import QtGui

def main():
    app = QtGui.QApplication(sys.argv)
    name = "Ruth"
    SetUpFileOperations.setUpBootFile()
    gui = SetUpWindow.SetUpWindow(app, 1000, 700, name)
    gui.setUpMenu()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
