#!/usr/bin/python
import wifi
import socket

def getWifiNetworks():
    networks = wifi.Cell.all('wlan0')
    return networks


def setWifiNetwork(ssid, password):
    networks = wifi.Cell.all('wlan0')
    cell = wifi.Cell.all('wlan0')[networks.index(ssid)]
    scheme = wifi.Scheme.for_cell('wlan0', ssid, cell, password.password)
    scheme.save()
    scheme.activate()


def checkWiFiConnection():
    REMOTE_SERVER = "www.google.com"
    try:
        host = socket.gethostbyname(REMOTE_SERVER)
        socket.create_connection((host, 80), 2)
        return True
    except:
        pass
    return False