#!/usr/bin/python
import time
import SetUpFileOperations
from PyQt4 import QtGui, QtCore
import imp
import Keyboard
import WiFiWindow

class SetUpWindow(QtGui.QMainWindow):
    def __init__(self, app, width, height, name):
        super(SetUpWindow, self).__init__()
        self.name = name
        self.defaultValues = ["Enter you name", "Enter WiFi name", "Enter WiFi password"]
        self.keyboardGUI = None
        self.windowWidth = width
        self.windowHeight = height
        self.password = passwordStorage.passwordStorage()
        self.selector = QtGui.QPushButton("", self)
        self.selectorPositionHorizontal = (self.windowWidth/2) - 103
        self.selectorPositionVertical = 146
        self.selectorPosition = 0
        self.selectorYMovement = 50
        self.selectorMax = 4
        self.setStyleSheet("background-color: RGBA(156, 254, 204, 255);")
        self.app = app
        self.setGeometry(100, 100, self.windowWidth, self.windowHeight)
        self.setWindowTitle(name.lower() + "StationOne")
        self.startUp()
        self.show()

    def startUp(self):
        self.hidingLabel = QtGui.QPushButton("", self)
        self.hidingLabel.setStyleSheet("background-color: rgba(255,255,255,255); color: rgba(255,255,255,255);")
        self.hidingLabel.resize(self.windowWidth + 20, self.windowHeight + 20)
        self.hidingLabel.move(-10, -10)

        self.setupLabel = QtGui.QPushButton("Initial Setup", self)
        self.setupLabel.resize(300, 60)
        self.setupLabel.setStyleSheet(
            "background-color: rgba(255,255,255,0); color: rgba(255,255,255,255); font-size:50px")
        self.setupLabel.move((self.windowWidth / 2) - 150, 50)

        self.nameTextBox = QtGui.QPushButton(self.defaultValues[0], self)
        self.nameTextBox.resize(200, 40)
        self.nameTextBox.setStyleSheet("background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black")
        self.nameTextBox.move((self.windowWidth/2) - 100, 150)

        self.wifiTextBox = QtGui.QPushButton(self.defaultValues[1], self)
        self.wifiTextBox.resize(200, 40)
        self.wifiTextBox.setStyleSheet("background-color: rgba(255,255,255,255); color: rgba(0,0,0,255); border-radius: 5px; border: 2px solid black")
        self.wifiTextBox.move((self.windowWidth/2) - 100, 200)

        self.continueButton = QtGui.QPushButton("Finish", self)
        self.continueButton.resize(200, 40)
        self.continueButton.setStyleSheet(
            "background-color: rgba(21, 29, 38, 255); color: rgba(255,255,255,255); border-radius: 5px;")
        self.continueButton.move((self.windowWidth / 2) - 100, 250)

        self.selector.setStyleSheet("background-color: rgba(0,0,0,0); border: 5px solid RGBA(253, 154, 203, 255); border-radius: 5px;")
        self.selector.resize(207.5,48.5)
        self.selector.move(self.selectorPositionHorizontal, self.selectorPositionVertical)

    def displayWelcome(self):
        self.app.processEvents()
        self.fadeIn(self.welcomeLabel)
        self.fadeOut(self.welcomeLabel)
        self.welcomeLabel.setText("ruthStationOne")
        time.sleep(2)
        self.fadeIn(self.welcomeLabel)
        self.fadeOut(self.welcomeLabel)

    def setUpMenu(self):
        #Configure menu buttons
        self.fadeOutHidingLabel(self.hidingLabel)


    def fadeIn(self, item):
        self.completed = 0
        while self.completed < 255:
            self.completed += 5
            item.setStyleSheet("font-size: 50px; background-color: rgba(0,0,0,0); color: rgba(0,0,0," + str(self.completed) + ");")
            time.sleep(0.01)
            self.app.processEvents()

    def fadeOut(self, item):
        self.completed = 260
        while self.completed > 0:
            self.completed -= 5
            item.setStyleSheet("font-size: 50px; background-color: rgba(0,0,0,0); color: rgba(0,0,0," + str(self.completed) + ");")
            time.sleep(0.01)
            self.app.processEvents()

    def fadeOutHidingLabel(self, item):
        self.completed = 260
        while self.completed > 0:
            self.completed -= 5
            item.setStyleSheet("background-color: rgba(255,255,255," + str(self.completed) + "); color: rgba(0,0,0,0);")
            time.sleep(0.01)
            self.app.processEvents()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_S:
            if self.selectorPosition < 2:
                self.selectorPositionVertical += self.selectorYMovement
                self.selector.move(self.selectorPositionHorizontal, self.selectorPositionVertical)
                self.selectorPosition += 1
        elif e.key() == QtCore.Qt.Key_W:
            if self.selectorPosition > 0:
                self.selectorPositionVertical -= self.selectorYMovement
                self.selector.move(self.selectorPositionHorizontal, self.selectorPositionVertical)
                self.selectorPosition -= 1
        elif e.key() == QtCore.Qt.Key_Return:
            if self.selectorPosition == 0:
                self.keyBoardGUI = Keyboard.Keyboard(self.app, self.nameTextBox, None, None, self.defaultValues)
            elif self.selectorPosition == 1:
                self.wifi = WiFiWindow.WiFiWindow(self.app, 550, 300, "")
            else:
                SetUpFileOperations.writeToSettingsFile("NAME", self.nameTextBox.text())
                SetUpFileOperations.writeToSettingsFile("WIFI", self.wifiTextBox.text())
                SetUpFileOperations.writeToSettingsFile("WIFIPASS", self.password.password)
