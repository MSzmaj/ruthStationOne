#!/usr/bin/python
import time
import LauncherButtonFunctions
from PyQt4 import QtGui, QtCore
import WiFi
import datetime


class LauncherWindow(QtGui.QMainWindow):
    def __init__(self, app, width, height):
        super(LauncherWindow, self).__init__()
        #self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.wifiConnected = WiFi.checkWiFiConnection()
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        screen_resolution = app.desktop().screenGeometry()
        self.screenWidth, self.screenHeight = screen_resolution.width(), screen_resolution.height()
        self.icons = Icons()
        self.styles = Styles()
        self.selectorPositionHorizontal = 0
        self.selectorPositionVertical = 0
        self.selectorYPosition = (height / 3) + 10
        self.appIconSize = 300
        self.selectorXPosition = ((self.screenWidth - (self.appIconSize * 3)) / 2) + 10
        self.selectorXMovement = 0
        self.selectorMax = 3
        #self.setStyleSheet(self.styles.mainBackgroundStyle)
        palette = QtGui.QPalette()
        palette.setBrush(QtGui.QPalette.Background, QtGui.QBrush(QtGui.QPixmap("images/background.png")))
        self.setPalette(palette)
        self.windowWidth = width
        self.windowHeight = height
        self.app = app
        self.setGeometry(0, 0, self.windowWidth, self.windowHeight)
        self.setWindowTitle("ruthStationOne")
        self.startUp()
        self.readyMenuBar()
        self.show()

    def startUp(self):
        #Initialise menu buttons
        self.selector = QtGui.QPushButton("", self)
        self.kodiButton = QtGui.QPushButton("", self)
        self.gameButton = QtGui.QPushButton("", self)
        self.musicButton = QtGui.QPushButton("", self)
        self.wifiButton = QtGui.QPushButton("", self)
        self.importButton = QtGui.QPushButton("", self)
        self.powerButton = QtGui.QPushButton("", self)
        self.restartButton = QtGui.QPushButton("", self)

        self.kodiButton.setStyleSheet(self.styles.buttonStyle)
        self.gameButton.setStyleSheet(self.styles.buttonStyle)
        self.musicButton.setStyleSheet(self.styles.buttonStyle)
        self.wifiButton.setStyleSheet(self.styles.buttonStyle)
        self.importButton.setStyleSheet(self.styles.buttonStyle)
        self.powerButton.setStyleSheet(self.styles.buttonStyle)
        self.restartButton.setStyleSheet(self.styles.buttonStyle)

        self.hidingLabel = QtGui.QPushButton("", self)
        self.hidingLabel.setStyleSheet(self.styles.hidingStyle)
        self.hidingLabel.resize(self.windowWidth + 20, self.windowHeight + 20)
        self.hidingLabel.move(-10, -10)

        #Set up welcome message
        font = QtGui.QFont("Droid")
        self.welcomeLabel = QtGui.QPushButton("Welcome to...", self)
        self.welcomeLabel.setFont(font)
        self.welcomeLabel.resize(self.windowWidth + 20, self.windowHeight + 20)
        self.welcomeLabel.move(-10, -10)
        self.welcomeLabel.setStyleSheet(self.styles.welcomeStyle)

    def readyMenuBar(self):
        self.wifiMenuBarIcon = QtGui.QPushButton("", self)
        self.timeIcon = QtGui.QPushButton("", self)

    def displayWelcome(self):
        self.app.processEvents()
        self.fadeIn(self.welcomeLabel)
        self.fadeOut(self.welcomeLabel)
        self.welcomeLabel.setText("ruthStationOne")
        time.sleep(2)
        self.fadeIn(self.welcomeLabel)
        self.fadeOut(self.welcomeLabel)

    def setUpMenu(self):
        #Configure button selector
        self.selector.setStyleSheet(self.styles.upperSelectorStyle)
        self.selector.resize(280, 280)
        self.selector.move(self.selectorXPosition, self.selectorYPosition)

        #Configure menu buttons
        self.setUpApps(self.appIconSize)
        self.setUpOptions(75, 230)
        self.fadeOutHidingLabel(self.hidingLabel)

        self.setUpMenuBar()

    def setUpMenuBar(self):
        totalFromTop = 10
        totalFromRight = 410
        size = 50
        time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        self.timeIcon.setText(time)
        self.timeIcon.setStyleSheet(self.styles.menuBarStyle)
        self.timeIcon.resize(400, 50)
        self.timeIcon.move(self.screenWidth - totalFromRight, totalFromTop)

        totalFromRight += 85

        connectionIcon = "Icons/connected.png"
        if not self.wifiConnected:
            connectionIcon = "Icons/notConnected.png"
        self.wifiMenuBarIcon.setStyleSheet(self.styles.menuBarStyle)
        self.wifiMenuBarIcon.setIcon(QtGui.QIcon(connectionIcon))
        self.wifiMenuBarIcon.setIconSize(QtCore.QSize(size, size))
        self.wifiMenuBarIcon.resize(size, size)
        self.wifiMenuBarIcon.move(self.screenWidth - totalFromRight, totalFromTop)


    def setUpApps(self, size):
        xCoor = (self.screenWidth - (size * 3)) / 2
        yCoor = self.windowHeight / 3

        gameIcon = QtGui.QPixmap(self.icons.gameIcon)
        kodiIcon = QtGui.QPixmap(self.icons.kodiIcon)
        musicIcon = QtGui.QPixmap(self.icons.musicIcon)

        self.gameButton.setIcon(QtGui.QIcon(gameIcon))
        self.kodiButton.setIcon(QtGui.QIcon(kodiIcon))
        self.musicButton.setIcon(QtGui.QIcon(musicIcon))

        self.gameButton.setIconSize(QtCore.QSize(300, 300))
        self.kodiButton.setIconSize(QtCore.QSize(300, 300))
        self.musicButton.setIconSize(QtCore.QSize(300, 300))

        self.gameButton.resize(size, size)
        self.kodiButton.resize(size, size)
        self.musicButton.resize(size, size)

        self.gameButton.move(xCoor, yCoor)
        self.kodiButton.move(xCoor + 300, yCoor)
        self.musicButton.move(xCoor + 600, yCoor)

        self.selectorXMovement = self.kodiButton.x() - self.gameButton.x()

    def setUpOptions(self, size, separation):
        xCoor = (self.screenWidth - (size * 3)) / 3
        yCoor = self.windowHeight / 1.5

        powerIcon = QtGui.QPixmap(self.icons.powerIcon)
        restartIcon = QtGui.QPixmap(self.icons.restartIcon)
        wifiIcon = QtGui.QPixmap(self.icons.wifiIcons)
        importIcon = QtGui.QPixmap(self.icons.importIcon)

        self.powerButton.setIcon(QtGui.QIcon(powerIcon))
        self.restartButton.setIcon(QtGui.QIcon(restartIcon))
        self.wifiButton.setIcon(QtGui.QIcon(wifiIcon))
        self.importButton.setIcon(QtGui.QIcon(importIcon))

        self.powerButton.setIconSize(QtCore.QSize(size, size))
        self.restartButton.setIconSize(QtCore.QSize(size, size))
        self.wifiButton.setIconSize(QtCore.QSize(size,size))
        self.importButton.setIconSize(QtCore.QSize(size, size))

        self.powerButton.resize(size, size)
        self.restartButton.resize(size, size)
        self.wifiButton.resize(size, size)
        self.importButton.resize(size, size)

        self.powerButton.move(xCoor, yCoor)
        self.restartButton.move(xCoor + (separation), yCoor)
        self.wifiButton.move(xCoor + (separation * 2), yCoor)
        self.importButton.move(xCoor + (separation * 3), yCoor)

    def fadeIn(self, item):
        self.completed = 0
        while self.completed < 255:
            self.completed += 5
            item.setStyleSheet("font-size: 50px; background-color: rgba(0,0,0,0); color: rgba(0,0,0," + str(self.completed) + ");")
            time.sleep(0.01)
            self.app.processEvents()

    def fadeOut(self, item):
        self.completed = 260
        while self.completed > 0:
            self.completed -= 5
            item.setStyleSheet("font-size: 50px; background-color: rgba(0,0,0,0); color: rgba(0,0,0," + str(self.completed) + ");")
            time.sleep(0.01)
            self.app.processEvents()

    def fadeOutHidingLabel(self, item):
        self.completed = 260
        while self.completed > 0:
            self.completed -= 5
            item.setStyleSheet("background-color: rgba(255,255,255," + str(self.completed) + "); color: rgba(0,0,0,0);")
            time.sleep(0.01)
            self.app.processEvents()

    def changeSelector(self):
        if self.selectorPositionVertical == 1:
            self.selector.setStyleSheet(self.styles.lowerSelectorStyle)
            self.selector.resize(100, 100)
            self.selectorYPosition = self.powerButton.y() - (self.powerButton.height() / 6)
            self.selectorXPosition = self.powerButton.x() - (self.powerButton.width() / 6)
            self.selectorXMovement = self.restartButton.x() - self.powerButton.x()
            self.selectorMax = 4
            self.selectorPositionHorizontal = 0
            self.selector.move(self.selectorXPosition, self.selectorYPosition)
        else:
            self.selector.setStyleSheet(self.styles.upperSelectorStyle)
            self.selector.resize(280, 280)
            self.selectorYPosition = (self.windowHeight / 3) + 10
            self.selectorXPosition = self.gameButton.x() + 10
            self.selectorXMovement = self.kodiButton.x() - self.gameButton.x()
            self.selectorMax = 3
            self.selectorPositionHorizontal = 0
            self.selector.move(self.selectorXPosition, self.selectorYPosition)

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_A:
            if self.selectorPositionHorizontal != 0:
                self.selector.move(self.selector.x() - self.selectorXMovement, self.selectorYPosition)
                self.selectorPositionHorizontal -= 1
        elif e.key() == QtCore.Qt.Key_D:
            if self.selectorPositionHorizontal != self.selectorMax - 1:
                self.selector.move(self.selector.x() + self.selectorXMovement, self.selectorYPosition)
                self.selectorPositionHorizontal += 1
        elif e.key() == QtCore.Qt.Key_S:
            if self.selectorPositionVertical != 1:
                self.selectorPositionVertical = 1
                self.changeSelector()
        elif e.key() == QtCore.Qt.Key_W:
            if self.selectorPositionVertical != 0:
                self.selectorPositionVertical = 0
                self.changeSelector()
        elif e.key() == QtCore.Qt.Key_Return:
            LauncherButtonFunctions.dispatch(self, self.app, self.selectorPositionHorizontal, self.selectorPositionVertical)


class Styles:
    def __init__(self):
        self.mainBackgroundStyle = "background-color: rgba(38, 152, 194, 110); outline: none;"
        self.buttonStyle = "background-color: rgba(0,0,0,0); outline: none;"
        self.hidingStyle = "background-color: rgba(255,255,255,255); color: rgba(255,255,255,255); outline: none;"
        self.welcomeStyle = "font-size: 50px; background-color: rgba(0,0,0,0); color: rgba(0,0,0,0); outline: none;"
        self.upperSelectorStyle = "background-color: rgba(0,0,0,0); border: 15px solid rgba(255,255,255,255); border-radius: 40px; outline: none;"
        self.lowerSelectorStyle = "background-color: rgba(68,60,119,255); border: 7px solid rgba(156, 254, 204, 255); border-radius: 30px; outline: none;"
        self.menuBarStyle = "font-size: 50px; background-color: rgba(0,0,0,0); color: rgba(255,255,255,255); outline: none;"

class Icons:
    def __init__(self):
        self.kodiIcon = "images/kodi.png"
        self.gameIcon = "images/game.png"
        self.musicIcon = "images/music.png"
        self.powerIcon = "images/power.png"
        self.restartIcon = "images/restart.png"
        self.wifiIcons = "images/wifi.png"
        self.importIcon = "images/import.png"
