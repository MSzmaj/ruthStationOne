ruthStationOne

An attempt to make a raspberryPi into a retro video-game console.
The actual emulation of retro games is done via retro-pi and the OS is the standard Raspian build.
This project contains set up script to install the necessary components and then installs the custom software that allows the user to use the system.

This project was an attempt to learn how to create visual applications using Python.
The software includes the following all built using Python:
1. Wi-Fi/Bluetooth access.
2. Custom made keyboard in order to work with a controller rather than mouse/keyboard.
3. Automatic software updates.
4. File transfer mechanism from external drives.
